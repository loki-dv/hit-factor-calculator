import { Component } from '@angular/core';
import { IData } from './models/data';
import { data } from './data/data';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Hit Factor Calculator';
  data: IData = data
}
