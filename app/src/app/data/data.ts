import { IData } from "../models/data"
export const data: IData = {
    "settings": {
        "stageName": "Stage 1",
        "perPaperTarget": 2
    },
    "time": 1.23,
    "paper": 1,
    "metall": 1,
    "c": 1,
    "d": 1,
    "m": 1,
    "pt": 0,
    "pe": 0,
    "hf": 0,
    "hfwif": 0,
    "lost": 0
}
