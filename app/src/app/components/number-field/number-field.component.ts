import { Component, Input } from "@angular/core";

@Component({
    selector: 'hfc-number-field',
    templateUrl: './number-field.component.html'
})
export class NumberFieldComponent {
    @Input() data: number
    @Input() label: string
}
