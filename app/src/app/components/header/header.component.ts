import { Component, Input } from "@angular/core";

@Component({
    selector: 'hfc-header',
    templateUrl: './header.component.html'
})
export class HeaderComponent {
    @Input() data: string
}
