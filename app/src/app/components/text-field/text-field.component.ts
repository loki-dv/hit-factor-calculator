import { Component, Input } from "@angular/core";

@Component({
    selector: 'hfc-text-field',
    templateUrl: './text-field.component.html'
})
export class TextFieldComponent {
    @Input() data: number
    @Input() label: string
}
