import { Component, Input } from "@angular/core";

@Component({
    selector: 'hfc-button',
    templateUrl: './button.component.html'
})
export class ButtonComponent {
    @Input() data: string
}
