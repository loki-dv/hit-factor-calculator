export interface IData {
    settings: {
        stageName: string
        perPaperTarget: number
    }
    time: number
    paper: number
    metall: number
    c: number
    d: number
    m: number
    pt: number
    pe: number
    hf: number
    hfwif: number
    lost: number
}
